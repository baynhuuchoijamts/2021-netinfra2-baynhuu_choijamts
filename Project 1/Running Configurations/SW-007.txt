Current configuration : 1434 bytes
!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
service password-encryption
!
hostname SW-007-BC
!
!
!
ip ssh version 2
no ip domain-lookup
ip domain-name SW-007
!
username baynachoijamts secret 5 $1$mERr$bYMWXQUG40sURk4ejUVic/
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
!
interface FastEthernet0/2
!
interface FastEthernet0/3
!
interface FastEthernet0/4
!
interface FastEthernet0/5
!
interface FastEthernet0/6
!
interface FastEthernet0/7
!
interface FastEthernet0/8
!
interface FastEthernet0/9
!
interface FastEthernet0/10
!
interface FastEthernet0/11
!
interface FastEthernet0/12
!
interface FastEthernet0/13
!
interface FastEthernet0/14
!
interface FastEthernet0/15
!
interface FastEthernet0/16
!
interface FastEthernet0/17
!
interface FastEthernet0/18
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
!
interface FastEthernet0/22
!
interface FastEthernet0/23
!
interface FastEthernet0/24
!
interface GigabitEthernet0/1
!
interface GigabitEthernet0/2
!
interface Vlan1
 no ip address
 shutdown
!
banner motd ^CUnauthorized access prohibited!^^C
!
!
!
line con 0
 logging synchronous
 exec-timeout 0 0
!
line vty 0 4
 access-class ONLY_ADMIN in
 exec-timeout 0 0
 logging synchronous
 login local
 transport input ssh
line vty 5 15
 access-class ONLY_ADMIN in
 login
 transport input ssh
!
!
!
!
end