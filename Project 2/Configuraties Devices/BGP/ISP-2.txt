version 15.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ISP-2-BC
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface GigabitEthernet0/0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface GigabitEthernet0/0/2
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/2/0
 ip address 10.48.10.1 255.255.255.252
!
interface Serial0/2/1
 ip address 2.48.2.1 255.255.255.252
 clock rate 2000000
!
interface Vlan1
 no ip address
 shutdown
!
router bgp 65531
 bgp log-neighbor-changes
 no synchronization
 neighbor 10.48.10.2 remote-as 65512
 network 2.48.2.0 mask 255.255.255.252
 network 10.48.10.0 mask 255.255.255.252
!
ip classless
!
ip flow-export version 9
!
!
!
no cdp run
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
ntp authentication-key 1 md5 0813594C0C170E161F1B 7
ntp authenticate
ntp server 209.165.80.2